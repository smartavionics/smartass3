
$fn = 50;

length = 82;

width = 70;

cr = 5;

topDepth = 14;

bottomDepth = 22;

skinDepth = 1;

speakerPillarBossDia = 8;
speakerPillarBossHeight = 5;

speakerMountingHoleSpacing = 53.74;

pcbMountingHoleSpacing = 50;

pcbPillarHeight = 22;

m3DiaVert = 3.6;

baseMountingHoleOffset = 25;

flangeDepth = 3;
flangeWidth = 10;
flangeMountingHoleSpacing = 40;

module roundedRect(l, w, h, cr) {
  hull() {
    for(sw=[-1, 1]) {
      for(sl=[-1, 1]) {
        translate([sl*(l/2-cr), sw*(w/2-cr), 0])
          cylinder(r=cr, h=h);
      }
    }
  }
}

module top() {
  difference() {
    union() {
      difference() {
        translate([(length-width)/2, 0, 0]) {
          // outer shell
          roundedRect(length, width, topDepth, cr);
        }
        translate([(length-width)/2, 0, 0]) {
          // inner volume
          translate([0, 0, skinDepth])
            roundedRect(length-4, width-4, topDepth, cr-2);
          // lip
          translate([0, 0, topDepth-1])
            roundedRect(length-2, width-2, 1.1, cr-1);
        }
        // sound holes
        for(sx = [-2, -1, 0, 1, 2, 4]) {
          for(sy = [-2, -1, 0, 1, 2]) {
            translate([sx*10, sy*10, -0.005]) {
              cylinder(d = 6, h = topDepth);
            }
          }
        }
      }
      // speaker pillars
      for(sx = [-1, 1]) {
        for(sy = [-1, 1]) {
          translate([sx*speakerMountingHoleSpacing/2, sy*speakerMountingHoleSpacing/2, 0]) {
            if(sx < 0)
              cylinder(d = 5.6, h = topDepth-2);
            cylinder(d = speakerPillarBossDia, h = speakerPillarBossHeight);
            // webs
            len = (width - speakerMountingHoleSpacing - speakerPillarBossDia)/2;
            translate([-2, sy*len - len/2, 0])
              cube([4, len, speakerPillarBossHeight]);
          }
        }
      }
    }
    // holes in speaker pillars
    for(sx = [-1, 1]) {
      for(sy = [-1, 1]) {
        translate([sx*speakerMountingHoleSpacing/2, sy*speakerMountingHoleSpacing/2, -0.005]) {
          cylinder(d = 6.6, h = speakerPillarBossHeight - 1 + 0.05);
          translate([0, 0, speakerPillarBossHeight - 1 + 0.2 + 0.05])
            cylinder(d = 3.3, h = topDepth);
        }
      }
    }
  }
}

module knockoutCutter(d, h, gap) {
  difference() {
    cylinder(d = d, h = h);
    cylinder(d = d-gap, h = h);
    for(o = [-d/2, d/2]) {
      translate([o-gap, 0-gap/2, 0])
        cube([gap*2, gap, h]);
      translate([0-gap/2, o-gap, 0])
        cube([gap, gap*2, h]);
    }
  }
}

module bottom() {

  bossHeight = 8;

  difference() {
    union() {
      translate([(length-width)/2, 0, 0]) {
        difference() {
          union() {
            // outer shell
            roundedRect(length, width, bottomDepth, cr);
            if(flangeWidth > 0) {
              // flange
              roundedRect(length+flangeWidth*2, width, flangeDepth, cr);
            }
          }
          // inner volume
          translate([0, 0, skinDepth])
            roundedRect(length-4, width-4, bottomDepth, cr-2);
        }
      }
      // inside pillars
      for(sx = [-1, 1]) {
        for(sy = [-1, 1]) {
          translate([sx*speakerMountingHoleSpacing/2, sy*speakerMountingHoleSpacing/2, 0]) {
            cylinder(d = 12.5, h = bossHeight);
            cylinder(d = 5.6, h = bottomDepth);
            // webs
            len = (width - speakerMountingHoleSpacing - speakerPillarBossDia)/2;
            translate([-2, sy*len - len/2, 0])
              cube([4, len, bossHeight]);
          }
        }
      }
      if(flangeWidth == 0) {
        for(sy = [-1, 1]) {
          // pads around holes in bottom
          translate([0, sy*baseMountingHoleOffset, 0]) {
            cylinder(d = 15, h = skinDepth*2);
          }
        }
      }
    }
    for(sx = [-1, 1]) {
      for(sy = [-1, 1]) {
        // holes in pillars
        translate([sx*speakerMountingHoleSpacing/2, sy*speakerMountingHoleSpacing/2, -0.005]) {
          // nut trap
          cylinder(h=bossHeight-2, d=6.6, $fn=6);
          translate([0, 0, bossHeight-2+0.2])
            cylinder(d = m3DiaVert, h = bottomDepth);
        }
        // holes in flange
        if(flangeWidth > 0) {
          translate([(length-width)/2 + sx*(length/2 + flangeWidth/2), sy*flangeMountingHoleSpacing/2, -0.005]) {
            cylinder(d = 4.8, flangeDepth+0.1);
          }
        }
      }
      if(flangeWidth == 0) {
        // mounting holes in bottom
        translate([0, sx*baseMountingHoleOffset, -0.005]) {
          cylinder(d = 4.2, skinDepth*3+0.1);
        }
      }
/*
      // cable exit holes
      for(x = [-20, 20]) {
        translate([x, 0, -0.005]) {
          cylinder(d = 5, skinDepth*3+0.1);
          knockoutCutter(15, skinDepth*3+0.1, 2);
        }
      }
*/
    }
    // connector hole in end
    translate([length/2+cr-1.6, -2.5, 3.5])
      #cube([2, 11, 8]);
  }
  // lip
  if(1)
  translate([(length-width)/2, 0, bottomDepth]) {
    difference() {
      roundedRect(length-2.5, width-2.5, 1, cr-0.25);
      translate([0, 0, -0.05])
        roundedRect(length-4, width-4, 1.1, cr-1.0);
    }
  }
}

pcbLen = 60;
pcbWidth = 30;

module pcb(th) {
  holesX = pcbMountingHoleSpacing/2;
  holesY = 11.5;
  difference() {
    translate([-pcbLen/2, -pcbWidth/2, -th/2])
      cube([pcbLen, pcbWidth, th]);
    for(x = [-holesX, holesX]) {
      translate([x, holesY, -th/2-0.01])
        cylinder(d = 3.2, h = th+0.2);
    }
  }
  // input connector
  translate([0, -pcbWidth/2 + 1, 0])
    cube([8, 6, 12]);
  for(x = [-holesX, holesX]) {
    translate([x, holesY, th/2])
      cylinder(d = 6, h = 2.4); // screw head
  }
}

module pcbMount(pcbOffset) {
  blockLen=8;
  blockWidth=8;
  blockHeight=8;
  difference() {
    // blocks
    for(sy = [-1, 1]) {
      hull() {
        translate([pcbOffset-blockLen, sy*pcbMountingHoleSpacing/2-blockWidth/2, 0]) {
          // blocks to screw PCB to
          cube([blockLen, blockWidth, blockHeight]);
        }
        translate([0, sy*(speakerMountingHoleSpacing)/2, 0]) {
          // speaker pillars
          cylinder(d = 5.6, h = topDepth-2-1);
        }
      }
    }
    // horizontal holes for PCB screws
    for(sy = [-1, 1]) {
      translate([pcbOffset-6, sy*pcbMountingHoleSpacing/2-blockWidth/2, 0.5]) {
        translate([0, blockWidth/2, 4])
          rotate([0, 90, 0])
            cylinder(d = 3, h= 8);
      }
    }
    // cutouts at bottom of speaker pillars
    for(y = [-speakerMountingHoleSpacing/2, speakerMountingHoleSpacing/2]) {
      translate([0, y, 0]) {
        // large diameter cutout to clear boss on inside of top
        difference() {
          cylinder(d=speakerPillarBossDia+2, h=speakerPillarBossHeight-skinDepth+0.2);
          // DIY support for the overhang
          difference() {
            cylinder(d=5.6, h=speakerPillarBossHeight-skinDepth);
            cylinder(d=4.5, h=speakerPillarBossHeight-skinDepth);
          }
        }
        // hole for M3 screw
        translate([0, 0, speakerPillarBossHeight-skinDepth+0.4])
          cylinder(d = m3DiaVert, h = topDepth);
      }
    }
  }
  // flat plate between blocks
  translate([pcbOffset-8, 0, 0])
    roundedRect(6, speakerMountingHoleSpacing-speakerPillarBossDia, 1, 1);
}

if(0)
  top();

if(0)
  translate([0, 0, topDepth+bottomDepth])
    //rotate([180, 0, 0])
      bottom();

if(1) {
  translate([speakerMountingHoleSpacing/2+14.8, 0, 0]) {
    %translate([0, 0, pcbWidth/2+skinDepth+1])
      rotate([-90, 0, 270])
        color("green")
          pcb(1.6);
  }
  translate([speakerMountingHoleSpacing/2, 0, skinDepth]) {
    pcbMount(14);
  }
}
