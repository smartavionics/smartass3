
$fn=32;

length = 80;
thickness = 2; // base thickness

h = 8.4; // height of piece that slots into case side above the base

module rounded_rect(l, w, h, cr) {
  hull() {
    for(sw=[-1, 1]) {
      for(sl=[-1, 1]) {
        translate([sl*(l/2-cr), sw*(w/2-cr), 0])
          cylinder(r=cr, h=h);
      }
    }
  }
}

difference() {
  intersection() {
    translate([0, length/2, 0]) {
      rotate([90, 0, 0]) {
        linear_extrude(height=length) {
          polygon(points = [
            [0, 0],
            [24, 0],
            [24, thickness],
            [23.25, thickness],
            [22, thickness+1.5],
            [20.25, thickness+1.5],
            [19, thickness],
            [14.5, thickness],
            [14.5, thickness+h],
            //[15, thickness+h],
            [16, thickness+h-2.1],
            [17.2, thickness+h-2.1],
            [17.2, thickness+h+5.3],
            [16.5, thickness+h+5.3],
            [15.25, thickness+h+4],
            [11, thickness+h+1.5],
            [7, thickness],
            [0, thickness]
          ]);
        }
      }
    }
    // give outboard portion of flange rounded corners
    translate([15, 0, 0])
      rounded_rect(30, length, 30, 3);
  }
  // holes
  for(sy = [-1, 1]) {
    translate([5, sy*30, 0]) {
      #cylinder(d=4.4, h=thickness);
      translate([0, 0, thickness])
        cylinder(d=10, h=20);
    }
  }
  // logo
  translate([8.25, 0, 4]) {
    rotate([0, -70, 0]) {
      rotate([0, 0, -90]) {
          #linear_extrude(height=4) {
            text("SmartASS-3", size = 6, halign="center", font = "Liberation Sans");
          }
      }
    }
  }
}

// support
if(0)for(sy = [-1, 1]) {
  t = 1;
  #hull() {
    translate([17.7, sy*(length/2-0.75), 0]) {
      cylinder(d=t, thickness);
    }
    translate([16.7, sy*(length/2-0.75), 0]) {
      cylinder(d=t, h=thickness+6.5);
    }
  }
}

 