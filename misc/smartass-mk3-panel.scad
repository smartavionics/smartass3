
$fn=32;

rotate([0, 180, 0]) {
  linear_extrude(height=1.2) {
    import("smartass-mk3-panel.dxf");
  }
  for(y = [3, 24]) {
    translate([54.25-45, y, -4])
      difference() {
        cube([90, 2, 5]);
        for(cx = [-5, 95]) {
          translate([cx, 5, -20])
            rotate([90, 0, 0])
              cylinder(h=10, r=25);
        }
      }
  }
}
